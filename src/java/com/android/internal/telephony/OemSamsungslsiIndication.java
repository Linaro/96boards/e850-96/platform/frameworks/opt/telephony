/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import android.hardware.radio.V1_0.RadioError;
import android.hardware.radio.V1_0.RadioIndicationType;
import android.hardware.radio.V1_0.RadioResponseType;
import android.hardware.radio.V1_2.CellConnectionStatus;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.HwBinder;
import android.os.Message;
import android.telephony.LteVopsSupportInfo;
import android.telephony.PhysicalChannelConfig;
import android.telephony.ServiceState;
import android.telephony.CellInfo;
import android.telephony.SignalStrength;

import com.android.internal.telephony.dataconnection.NasTimerResponse;
import com.android.internal.telephony.uicc.IccUtils;

import vendor.samsung_slsi.telephony.hardware.radio.V1_1.IOemSamsungslsiIndication;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


import static com.android.internal.telephony.OemRILConstants.*;
import static com.android.internal.telephony.RILConstants.RIL_UNSOL_SIGNAL_STRENGTH;
import static com.android.internal.telephony.RILConstants.RIL_UNSOL_CELL_INFO_LIST;

/**
 * {@hide}
 */
class OemSamsungsliIndication extends IOemSamsungslsiIndication.Stub {

    RIL mRil;

    OemSamsungsliIndication(RIL ril) {
        mRil = ril;
    }

    /**
     * @param indicationType RadioIndicationType
     * @param data Data sent by oem
     */
    @Override
    public void oemHookRaw(int indicationType, ArrayList<Byte> data) {
        mRil.processIndication(indicationType);

        byte[] response = RIL.arrayListToPrimitiveArray(data);

        if (mRil.mUnsolOemHookRawRegistrant != null) {
            mRil.mUnsolOemHookRawRegistrant.notifyRegistrant(new AsyncResult(null, response, null));
        }
    }

    @Override
    public void suppSvcReturnResult(int indicationType, String result) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_SUPP_SVC_RETURN_RESULT);

        if (mRil.mSSResponseDataRegistrants != null) {
            mRil.mSSResponseDataRegistrants.notifyRegistrants(new AsyncResult(null, result, null));
        }
    }

    @Override
    public void callPresentInd(int indicationType) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_CALL_PRESENT_IND);

        // TODO
    }

    /**
     * (TBD)
     *
     */
    @Override
    public void wbAmrReportInd(int indicationType, boolean on) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLogRet(RIL_UNSOL_WB_AMR_REPORT_IND, on);

        if (mRil.mWbAmrReportRegistrant != null) {
            mRil.mWbAmrReportRegistrant.notifyRegistrant(new AsyncResult(null, on, null));
        }
    }

    /**
     * (TBD)
     *
     */
    @Override
    public void vsimOperationInd(int indicationType,
            vendor.samsung_slsi.telephony.hardware.radio.V1_0.VsimOperationEvent event) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_VSIM_OPERATION_INDICATION);

        // TODO
    }

    /**
     * (TBD)
     *
     */

    @Override
    public void nasTimerStatusInd(int indicationType,
            vendor.samsung_slsi.telephony.hardware.radio.V1_0.NasTimerStatus nasTimer) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_NAS_TIMER_STATUS_IND);

        NasTimerResponse response = new NasTimerResponse();
        response.type = nasTimer.type;
        response.status = nasTimer.status;
        response.value = nasTimer.value;
        response.apn = nasTimer.apn;

        if (mRil.mDataNasTimerStatusRegistrants != null) {
            mRil.mDataNasTimerStatusRegistrants.notifyRegistrants(new AsyncResult(null, response, null));
        }
    }

    /**
     * (TBD)
     *
     */
    @Override
    public void emergencyActInd(int indicationType, int act, int status) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_EMERGENCY_ACT_INFO);
        int response[] = new int[2];
        response[0] = act;
        response[1] = status;

        if (mRil.mEmergencyActInfoRegistrant != null) {
            mRil.mEmergencyActInfoRegistrant.notifyRegistrants(new AsyncResult(null, response, null));
        }
    }

    /**
     * (TBD)
     *
     */
    @Override
    public void onUssdWithDcsInd(int indicationType, String ussdModeType, String msg, String dcs) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_ON_USSD_WITH_DCS);

        String[] response = new String[3];
        response[0] = ussdModeType;
        response[1] = msg;
        response[2] = dcs;

        int ussdDcs = Integer.parseInt(response[2], 16);
        if (RIL.RILJ_LOGD) mRil.riljLog("[UNSL] USSD dcs : " + ussdDcs + " recv.string: " + response[1]);
        if ( ussdDcs == 148 ) {
            //EUC-KR
            try {
                byte[] ussdbytes = IccUtils.hexStringToBytes(response[1]);
                Charset charset = Charset.forName("EUC-KR");
                String euckr = new String(ussdbytes, charset);
                response[1] = euckr;
            } catch(Exception e) {
                if (RIL.RILJ_LOGD) mRil.riljLog("[UNSL] USSD_exception : " + e);
            }
            if (RIL.RILJ_LOGD) mRil.riljLog("[UNSL] USSD_euckr : " + response[1]);
        }

        if (mRil.mUSSDRegistrant != null) {
            mRil.mUSSDRegistrant.notifyRegistrant(new AsyncResult (null, response, null));
        }
    }


    /**
     * (TBD)
     *
     */
    @Override
    public void volteAvailableInfoInd(int indicationType, int volteAvailable, int emcAvailable) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_VOLTE_AVAILABLE_INFO);
        int response[] = new int[2];
        response[0] = volteAvailable;
        response[1] = emcAvailable;

        if (mRil.mVolteAvailableInfoRegistrant != null) {
            mRil.mVolteAvailableInfoRegistrant.notifyRegistrants(new AsyncResult(null, response, null));
        }
    }

    /**
     * (TBD)
     *
     */
    @Override
    public void iccIdInfoInd(int indicationType, String iccIdInfo) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_ICCID_INFO);

        if (mRil.mIccIdInfoRegistrant != null) {
            mRil.mIccIdInfoRegistrant.notifyRegistrant(new AsyncResult(null, iccIdInfo, null));
        }
    }

    @Override
    public void emergencySupportRatModeInd(int indicationType, int supportRatMode) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_EMERGENCY_SUPPORT_RAT_MODE);

        if (mRil.mEmergencySupportRatModeRegistrant != null) {
            mRil.mEmergencySupportRatModeRegistrant.notifyRegistrant(new AsyncResult(null, supportRatMode, null));
        }
    }

    /**
     * (TBD)
     *
     */
    @Override
    public void ussdCanceledInd(int indicationType) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) mRil.unsljLog(RIL_UNSOL_USSD_CANCELED);

        if (mRil.mUssdCanceledRegistrants != null) {
            mRil.mUssdCanceledRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
        }
    }

    // @1.1 implement
    private int convertConnectionStatusFromCellConnectionStatus(int status) {
        switch (status) {
            case CellConnectionStatus.PRIMARY_SERVING:
                return PhysicalChannelConfig.CONNECTION_PRIMARY_SERVING;
            case CellConnectionStatus.SECONDARY_SERVING:
                return PhysicalChannelConfig.CONNECTION_SECONDARY_SERVING;
            default:
                // only PRIMARY_SERVING and SECONDARY_SERVING are supported.
                mRil.riljLoge("Unsupported CellConnectionStatus in PhysicalChannelConfig: "
                        + status);
                return PhysicalChannelConfig.CONNECTION_UNKNOWN;
        }
    }

    /**
    * Indicates current physical channel configuration.
    */
    @Override
    public void currentNrPhysicalChannelConfigs(int indicationType, int rat, int status) {
        // deprecated
    }

    /**
     * Indicates the current signal strength of the camped or primary serving cell.
     */
    @Override
    public void currentSignalStrength(int indicationType,
                                      vendor.samsung_slsi.telephony.hardware.radio.V1_1.SignalStrength signalStrength) {
       // deprecated
    }

    /** Get unsolicited message for cellInfoList using OEM HAL V1.1 */
    public void cellInfoList(int indicationType,
                                 ArrayList<vendor.samsung_slsi.telephony.hardware.radio.V1_1.CellInfo> records) {
        // deprecated
    }

    public void endcCapabilityInd(int indicationType, int capability, int cause) {
        mRil.processIndication(indicationType);

        if (RIL.RILJ_LOGD) unsljLog(RIL_UNSOL_IND_ENDC_CAPABILITY);
        int response[] = new int[2];
        response[0] = capability;
        response[1] = cause;

        if (mRil.mEndcCapabilityRegistrant != null) {
            mRil.mEndcCapabilityRegistrant.notifyRegistrant(new AsyncResult(null, response, null));
        }
    }

    static String responseToString(int request) {
        switch (request) {
        case RIL_UNSOL_NR_PHYSICAL_CHANNEL_CONFIGS:
            return "RIL_UNSOL_NR_PHYSICAL_CHANNEL_CONFIGS";
        case RIL_UNSOL_IND_ENDC_CAPABILITY:
            return "RIL_UNSOL_IND_ENDC_CAPABILITY";
        default:
            return "<unknown response>";
        }
    }

    void unsljLog(int response) {
        mRil.riljLog("[UNSL]< " + responseToString(response));
    }

    void unsljLogRet(int response, Object ret) {
        mRil.riljLog("[UNSL]< " + responseToString(response) + " " + retToString(response, ret));
    }

    static String retToString(int req, Object ret) {
        if (ret == null) return "";
        return ret.toString();
    }
}
