/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import android.hardware.radio.V1_0.RadioError;
import android.hardware.radio.V1_0.RadioResponseInfo;
import android.hardware.radio.V1_0.RadioResponseType;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.HwBinder;
import android.os.Message;
import android.telephony.Rlog;
import java.util.ArrayList;
import vendor.samsung_slsi.telephony.hardware.radio.V1_0.*;
import vendor.samsung_slsi.telephony.hardware.radio.V1_1.IOemSamsungslsiResponse;
import vendor.samsung_slsi.telephony.hardware.radio.V1_1.DataRegStateResult;


import static com.android.internal.telephony.OemRILConstants.*;

/**
 * {@hide}
 */
class OemSamsungsliResponse extends IOemSamsungslsiResponse.Stub {
    RIL mRil;
    OemSamsungsliResponse(RIL ril) {
        mRil = ril;
    }

    private void responseInts(RadioResponseInfo responseInfo, int ...var) {
        final ArrayList<Integer> ints = new ArrayList<>();
        for (int i = 0; i < var.length; i++) {
            ints.add(var[i]);
        }
        responseIntArrayList(responseInfo, ints);
    }

    private void responseIntArrayList(RadioResponseInfo responseInfo, ArrayList<Integer> var) {
        RILRequest rr = mRil.processResponse(responseInfo);

        if (rr != null) {
            Object ret = null;
            if (responseInfo.error == RadioError.NONE) {
                int[] response = new int[var.size()];
                for (int i = 0; i < var.size(); i++) {
                    response[i] = var.get(i);
                }
                ret = response;
                sendMessageResponse(rr.mResult, ret);
            }
            mRil.processResponseDone(rr, responseInfo, ret);
        }
    }

    private void responseString(RadioResponseInfo responseInfo, String str) {
        RILRequest rr = mRil.processResponse(responseInfo);

        if (rr != null) {
            String ret = null;
            if (responseInfo.error == RadioError.NONE) {
                ret = str;
                sendMessageResponse(rr.mResult, ret);
            }
            mRil.processResponseDone(rr, responseInfo, ret);
        }
    }

    private void responseStrings(RadioResponseInfo responseInfo, String ...str) {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < str.length; i++) {
            strings.add(str[i]);
        }
        responseStringArrayList(mRil, responseInfo, strings);
    }

    static void responseStringArrayList(RIL ril, RadioResponseInfo responseInfo,
                                        ArrayList<String> strings) {
        RILRequest rr = ril.processResponse(responseInfo);

        if (rr != null) {
            String[] ret = null;
            if (responseInfo.error == RadioError.NONE) {
                ret = new String[strings.size()];
                for (int i = 0; i < strings.size(); i++) {
                    ret[i] = strings.get(i);
                }
                sendMessageResponse(rr.mResult, ret);
            }
            ril.processResponseDone(rr, responseInfo, ret);
        }
    }

    private void responseBoolean(RadioResponseInfo responseInfo, boolean status) {
        RILRequest rr = mRil.processResponse(responseInfo);

        if (rr != null) {
            Boolean ret = status;
            if (responseInfo.error == RadioError.NONE) {
                sendMessageResponse(rr.mResult, ret);
            }
            mRil.processResponseDone(rr, responseInfo, ret);
        }
    }

    private void responseOperatorInfoExs(RadioResponseInfo responseInfo,
                                       ArrayList<vendor.samsung_slsi.telephony.hardware.radio.V1_0.OperatorInfoEx>
                                               networkInfos) {
        RILRequest rr = mRil.processResponse(responseInfo);

        if (rr != null) {
            ArrayList<OperatorInfo> ret = new ArrayList<OperatorInfo>();
            for (int i = 0; i < networkInfos.size(); i++) {
                ret.add(new OperatorInfo(networkInfos.get(i).alphaLong,
                    networkInfos.get(i).alphaShort, networkInfos.get(i).operatorNumeric,
                    convertOpertatorInfoToString(networkInfos.get(i).status), String.valueOf(networkInfos.get(i).rat)));
            }
            if (responseInfo.error == RadioError.NONE) {
                sendMessageResponse(rr.mResult, ret);
            }
            mRil.processResponseDone(rr, responseInfo, ret);
        }
    }

    private void responseUplmnInfos(RadioResponseInfo responseInfo, int max,
                                       ArrayList<vendor.samsung_slsi.telephony.hardware.radio.V1_0.UplmnInfo>
                                               uplmnInfos) {
        RILRequest rr = mRil.processResponse(responseInfo);

        if (rr != null) {
            UplmnInfoSet ret = new UplmnInfoSet(max, uplmnInfos);
            if (responseInfo.error == RadioError.NONE) {
                sendMessageResponse(rr.mResult, ret);
            }
            mRil.processResponseDone(rr, responseInfo, ret);
        }
    }

    /**
     * Helper function to send response msg
     * @param msg Response message to be sent
     * @param ret Return object to be included in the response message
     */
    static void sendMessageResponse(Message msg, Object ret) {
        if (msg != null) {
            AsyncResult.forMessage(msg, ret, null);
            msg.sendToTarget();
        }
    }

    private void responseVoid(RadioResponseInfo responseInfo) {
        RILRequest rr = mRil.processResponse(responseInfo);

        if (rr != null) {
            Object ret = null;
            if (responseInfo.error == RadioError.NONE) {
                sendMessageResponse(rr.mResult, ret);
            }
            mRil.processResponseDone(rr, responseInfo, ret);
        }
    }

    /**
     * Replace IOemHook (deprecated)
     * @param responseInfo Response info struct containing response type, serial no. and error
     * @param data Data returned by oem
     */
    @Override
    public void sendRequestRawResponse(RadioResponseInfo responseInfo, ArrayList<Byte> data) {
        RILRequest rr = mRil.processResponse(responseInfo);

        if (rr != null) {
            byte[] ret = null;
            if (responseInfo.error == RadioError.NONE) {
                ret = RIL.arrayListToPrimitiveArray(data);
                RadioResponse.sendMessageResponse(rr.mResult, ret);
            }
            mRil.processResponseDone(rr, responseInfo, ret);
        }
    }

    @Override
    public void generalResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void queryCOLPResponse(RadioResponseInfo responseInfo, int status) {
        responseInts(responseInfo, status);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void queryCOLRResponse(RadioResponseInfo responseInfo, int status) {
        responseInts(responseInfo, status);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void iccGetAtrResponse(RadioResponseInfo responseInfo, String atr) {
        responseString(responseInfo, atr);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void sendUSSDWithDcsResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setPreferredUplmnResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void getPreferredUplmnResponse(RadioResponseInfo responseInfo, int max, ArrayList<vendor.samsung_slsi.telephony.hardware.radio.V1_0.UplmnInfo> uplmnInfo) {
        responseUplmnInfos(responseInfo, max, uplmnInfo);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setEmcStatusResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setFemtoCellSearchResponse(RadioResponseInfo responseInfo, int result, String mccmnc) {
        //TODO:
        responseStrings(responseInfo, String.valueOf(result), mccmnc);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setCdmaHybridModeResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }
    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void getCdmaHybridModeResponse(RadioResponseInfo responseInfo, int mode) {
        responseInts(responseInfo, mode);
    }

   /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     * @param networkInfos List of network operator information as OperatorInfos defined in
     * types.hal
     */
    @Override
    public void queryBplmnSearchResponse(RadioResponseInfo responseInfo,
                                            ArrayList<vendor.samsung_slsi.telephony.hardware.radio.V1_0.OperatorInfoEx>
                                                        networkInfos) {
       responseOperatorInfoExs(responseInfo, networkInfos);
   }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setNetworkSelectionModeManualResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void dialWithCallTypeResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setBarringPasswordOverMmiResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setDualNetworkTypeAndAllowDataResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setDsNetworkTypeResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    @Override
    public void setVoiceOperationResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    public void deactivateDataCallWithReasonResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    public void setIntPsServiceResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    private static String convertOpertatorInfoToString(int status) {
        if (status == android.hardware.radio.V1_0.OperatorStatus.UNKNOWN) {
            return "unknown";
        } else if (status == android.hardware.radio.V1_0.OperatorStatus.AVAILABLE) {
            return "available";
        } else if (status == android.hardware.radio.V1_0.OperatorStatus.CURRENT) {
            return "current";
        } else if (status == android.hardware.radio.V1_0.OperatorStatus.FORBIDDEN) {
            return "forbidden";
        } else {
            return "";
        }
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    public void emulateIndResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param info Response info struct containing response type, serial no. and error
     * @param policy SIM lock policy 0 to 11
     * @param status 0: unlocked, 1: locked
     * @lockType 0: unlocked, 1: Network Lock(PN), 2: Network Subset Lock(PU),
     * @param maxRetryCount max retry count
     * @param remainCount remaining unlock retry count
     * @param list of lock code as ASCII string
     */
    public void getSimLockStatusResponse(RadioResponseInfo responseInfo, int policy, int status, int lockType,
                    int maxRetryCount, int remainCount, ArrayList<String> lockCode) {
        RILRequest rr = mRil.processResponse(responseInfo);
        if (rr != null) {
            SimLockStatus ret = new SimLockStatus(policy, status, lockType, maxRetryCount, remainCount, lockCode);
            if (responseInfo.error == RadioError.NONE) {
                sendMessageResponse(rr.mResult, ret);
            }
            mRil.processResponseDone(rr, responseInfo, ret);
        }
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    public void sendVsimNotificationResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     */
    public void sendVsimOperationResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and errori
     * @param status 0: fail, 1: success
     */
    public void setActivateVsimResponse(RadioResponseInfo responseInfo, int status) {
        responseInts(responseInfo, status);
    }

    /**
     * @param info Response info struct containing response type, serial no. and error
     */
    public void setEndcModeResponse(RadioResponseInfo responseInfo) {
        responseVoid(responseInfo);
    }

    /**
     * @param info Response info struct containing response type, serial no. and error
     * @param enable = true EN-DC enabled, enable = false EN-DC disabled
     */
    public void getEndcModeResponse(RadioResponseInfo responseInfo, boolean enable) {
        responseBoolean(responseInfo, enable);
    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     * @param dataRegResponse Current Data registration response as defined by DataRegStateResult in
     *        types.hal
     */
    public void getDataRegistrationStateResponse(RadioResponseInfo responseInfo, DataRegStateResult dataRegResponse) {

    }

    /**
     * @param signalStrength Current signal strength
     *
     * Valid errors returned:
     *   RadioError:NONE
     *   RadioError:RADIO_NOT_AVAILABLE
     *   RadioError:INTERNAL_ERR
     */
    public void getSignalStrengthResponse(RadioResponseInfo responseInfo, vendor.samsung_slsi.telephony.hardware.radio.V1_1.SignalStrength signalStrength) {

    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error.
     * @param cellInfo List of current cell information known to radio.
     */
    public void getCellInfoListResponse(RadioResponseInfo responseInfo, ArrayList<vendor.samsung_slsi.telephony.hardware.radio.V1_1.CellInfo> cellInfo) {

    }

    /**
     * @param responseInfo Response info struct containing response type, serial no. and error
     * @param resp capacity information of sim
     */
    public void getSmsStorageOnSimResponse(RadioResponseInfo responseInfo, int[] resp) {
        responseInts(responseInfo, resp);
    }
}
