/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import android.annotation.UnsupportedAppUsage;
import android.content.pm.PackageManager;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.Rlog;

import com.android.internal.telephony.uicc.IccCardApplicationStatus;
import com.android.internal.telephony.uicc.AdnRecord;
import com.android.internal.telephony.uicc.AdnRecordCache;
import com.android.internal.telephony.uicc.IccCardApplicationStatus.AppType;
import com.android.internal.telephony.uicc.IccConstants;
import com.android.internal.telephony.uicc.IccFileHandler;
import com.android.internal.telephony.uicc.IccRecords;
import com.android.internal.telephony.uicc.IccUtils;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.ContentValues;
import android.telephony.Rlog;
import android.text.TextUtils;

/**
 * SimPhoneBookInterfaceManager to provide an inter-process communication to
 * access ADN-like SIM records.
 */
public class IccPhoneBookInterfaceManager {
    static final String LOG_TAG = "IccPhoneBookIM";
    @UnsupportedAppUsage
    protected static final boolean DBG = true;

    @UnsupportedAppUsage
    protected Phone mPhone;
    @UnsupportedAppUsage
    protected AdnRecordCache mAdnCache;

    protected static final int EVENT_GET_SIZE_DONE = 1;
    protected static final int EVENT_LOAD_DONE = 2;
    protected static final int EVENT_UPDATE_DONE = 3;
    protected static final int EVENT_LOAD_SDN_DONE = 4;
    protected static final int EVENT_LOAD_PURE_ADN_DONE = 5;

    /* If PBR records don't exist but ADN records exist, mIsPbrExist will be false. 
       Init this: 'true' because device must try to read PBR records first */
    private boolean mIsPbrExist = true;
    private static int mTagLen = -1;
    private static final int FOOTER_SIZE_BYTES = 14;

    private static final class Request {
        AtomicBoolean mStatus = new AtomicBoolean(false);
        Object mResult = null;
    }

    @UnsupportedAppUsage
    protected Handler mBaseHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;
            Request request = (Request) ar.userObj;

            switch (msg.what) {
                case EVENT_GET_SIZE_DONE:
                    int[] recordSize = null;
                    if (ar.exception == null) {
                        recordSize = (int[]) ar.result;
                        // recordSize[0]  is the record length
                        // recordSize[1]  is the total length of the EF file
                        // recordSize[2]  is the number of records in the EF file
                        logd("GET_RECORD_SIZE Size " + recordSize[0]
                                + " total " + recordSize[1]
                                + " #record " + recordSize[2]);
                        mTagLen = recordSize[0] - FOOTER_SIZE_BYTES;
                    } else {
                        loge("EVENT_GET_SIZE_DONE: failed; ex=" + ar.exception);
                    }
                    notifyPending(request, recordSize);
                    break;
                case EVENT_UPDATE_DONE:
                    boolean success = (ar.exception == null);
                    if (!success) {
                        loge("EVENT_UPDATE_DONE - failed; ex=" + ar.exception);
                    }
                    notifyPending(request, success);
                    break;
                case EVENT_LOAD_DONE:
                    List<AdnRecord> records = null;
                    if (ar.exception == null) {
                        records = (List<AdnRecord>) ar.result;
                        mIsPbrExist = true;
                    } else {
                        loge("EVENT_LOAD_DONE: Cannot load PBR records; ex="
                                + ar.exception);
                        mIsPbrExist = false;
                    }
                    notifyPending(request, records);
                    break;
                case EVENT_LOAD_SDN_DONE:
                    List<AdnRecord> sdnRecords = null;
                    if (ar.exception == null) {
                        sdnRecords = (List<AdnRecord>) ar.result;
                    } else {
                        if(DBG) logd("Cannot load SDN records; ex="
                                + ar.exception);
                    }
                    notifyPending(request, sdnRecords);
                    break;
                case EVENT_LOAD_PURE_ADN_DONE:
                    List<AdnRecord> adnRecords = null;
                    if (ar.exception == null) {
                        adnRecords = (List<AdnRecord>) ar.result;
                    } else {
                        loge("EVENT_LOAD_PURE_ADN_DONE: Cannot load ADN records; ex="
                                + ar.exception);
                    }
                    notifyPending(request, adnRecords);
                    break;
            }
        }

        private void notifyPending(Request request, Object result) {
            if (request != null) {
                synchronized (request) {
                    request.mResult = result;
                    request.mStatus.set(true);
                    request.notifyAll();
                }
            }
        }
    };

    public IccPhoneBookInterfaceManager(Phone phone) {
        this.mPhone = phone;
        IccRecords r = phone.getIccRecords();
        if (r != null) {
            mAdnCache = r.getAdnCache();
        }
    }

    public void dispose() {
    }

    public void updateIccRecords(IccRecords iccRecords) {
        /* If SIM is removed, then mIsPbrExist will be initialized to 'true' */
        mIsPbrExist = true;
        if (iccRecords != null) {
            mAdnCache = iccRecords.getAdnCache();
        } else {
            mAdnCache = null;
        }
    }

    @UnsupportedAppUsage
    protected void logd(String msg) {
        Rlog.d(LOG_TAG, "[IccPbInterfaceManager] " + msg);
    }

    @UnsupportedAppUsage
    protected void loge(String msg) {
        Rlog.e(LOG_TAG, "[IccPbInterfaceManager] " + msg);
    }

    /**
     * Replace oldAdn with newAdn in ADN-like record in EF
     *
     * getAdnRecordsInEf must be called at least once before this function,
     * otherwise an error will be returned. Currently the email field
     * if set in the ADN record is ignored.
     * throws SecurityException if no WRITE_CONTACTS permission
     *
     * @param efid must be one among EF_ADN, EF_FDN, and EF_SDN
     * @param oldTag adn tag to be replaced
     * @param oldPhoneNumber adn number to be replaced
     *        Set both oldTag and oldPhoneNubmer to "" means to replace an
     *        empty record, aka, insert new record
     * @param newTag adn tag to be stored
     * @param newPhoneNumber adn number ot be stored
     *        Set both newTag and newPhoneNubmer to "" means to replace the old
     *        record with empty one, aka, delete old record
     * @param pin2 required to update EF_FDN, otherwise must be null
     * @return true for success
     */
    public boolean
    updateAdnRecordsInEfBySearch (int efid,
            String oldTag, String oldPhoneNumber,
            String newTag, String newPhoneNumber, String pin2) {


        if (mPhone.getContext().checkCallingOrSelfPermission(
                android.Manifest.permission.WRITE_CONTACTS)
            != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException(
                    "Requires android.permission.WRITE_CONTACTS permission");
        }


        if (DBG) logd("updateAdnRecordsInEfBySearch: efid=0x" +
                Integer.toHexString(efid).toUpperCase() + " ("+ Rlog.pii(LOG_TAG, oldTag) + "," +
                Rlog.pii(LOG_TAG, oldPhoneNumber) + ")" + "==>" + " ("+ Rlog.pii(LOG_TAG, newTag) +
                "," + Rlog.pii(LOG_TAG, newPhoneNumber) + ")"+ " pin2=" + Rlog.pii(LOG_TAG, pin2));

        efid = updateEfForIccType(efid);

        if (!checkLengthOfTag(efid, newTag)) return false;

        checkThread();
        Request updateRequest = new Request();
        synchronized (updateRequest) {
            Message response = mBaseHandler.obtainMessage(EVENT_UPDATE_DONE, updateRequest);
            AdnRecord oldAdn = new AdnRecord(oldTag, oldPhoneNumber);
            AdnRecord newAdn = new AdnRecord(newTag, newPhoneNumber);
            if (mAdnCache != null) {
                mAdnCache.updateAdnBySearch(efid, oldAdn, newAdn, pin2, response);
                waitForResult(updateRequest);
            } else {
                loge("Failure while trying to update by search due to uninitialised adncache");
            }
        }
        return (boolean) updateRequest.mResult;
    }

    public boolean updateAdnRecordsWithContentValuesInEfBySearch(int efid, ContentValues values,
            String pin2) {

        if (mPhone.getContext().checkCallingOrSelfPermission(
                android.Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException("Requires android.permission.WRITE_CONTACTS permission");
        }

        if (values == null) {
            loge("Failure because ContentValues is null");
            return false;
        }

        String oldTag = values.getAsString(IccProvider.STR_TAG);
        String newTag = values.getAsString(IccProvider.STR_NEW_TAG);
        String oldPhoneNumber = values.getAsString(IccProvider.STR_NUMBER);
        String newPhoneNumber = values.getAsString(IccProvider.STR_NEW_NUMBER);
        String oldEmail = values.getAsString(IccProvider.STR_EMAILS);
        String newEmail = values.getAsString(IccProvider.STR_NEW_EMAILS);
        String oldAnr = values.getAsString(IccProvider.STR_ANRS);
        String newAnr = values.getAsString(IccProvider.STR_NEW_ANRS);
        String oldAas = values.getAsString(IccProvider.STR_AASS);
        String newAas = values.getAsString(IccProvider.STR_NEW_AASS);
        String[] oldEmailArray = TextUtils.isEmpty(oldEmail) ? null : getStringArray(oldEmail);
        String[] newEmailArray = TextUtils.isEmpty(newEmail) ? null : getStringArray(newEmail);
        String[] oldAnrArray = TextUtils.isEmpty(oldAnr) ? null : getStringArray(oldAnr);
        String[] newAnrArray = TextUtils.isEmpty(newAnr) ? null : getStringArray(newAnr);
        String[] oldAasArray = TextUtils.isEmpty(oldAas) ? null : getStringArray(oldAas);
        String[] newAasArray = TextUtils.isEmpty(newAas) ? null : getStringArray(newAas);
        efid = updateEfForIccType(efid);
        if (DBG)
            logd("updateAdnRecordsWithContentValuesInEfBySearch: efid=" + efid + ", values = " + values + ", pin2="
                    + pin2);

        if (!checkLengthOfTag(efid, newTag)) return false;

        checkThread();
        Request updateRequest = new Request();
        synchronized (updateRequest) {
            Message response = mBaseHandler.obtainMessage(EVENT_UPDATE_DONE, updateRequest);
            AdnRecord oldAdn;
            AdnRecord newAdn;
            if (efid == IccConstants.EF_PBR) {
                oldAdn = new AdnRecord(oldTag, oldPhoneNumber, oldEmailArray, oldAnrArray);
                newAdn = new AdnRecord(newTag, newPhoneNumber, newEmailArray, newAnrArray);
            } else {
                oldAdn = new AdnRecord(oldTag, oldPhoneNumber);
                newAdn = new AdnRecord(newTag, newPhoneNumber);
            }
            if (mAdnCache != null) {
                mAdnCache.updateAdnBySearch(efid, oldAdn, newAdn, pin2, response);
                waitForResult(updateRequest);
            } else {
                loge("Failure while trying to update by search due to uninitialised adncache");
            }
        }
        return (boolean) updateRequest.mResult;
    }

    /**
     * Update an ADN-like EF record by record index
     *
     * This is useful for iteration the whole ADN file, such as write the whole
     * phone book or erase/format the whole phonebook. Currently the email field
     * if set in the ADN record is ignored.
     * throws SecurityException if no WRITE_CONTACTS permission
     *
     * @param efid must be one among EF_ADN, EF_FDN, and EF_SDN
     * @param newTag adn tag to be stored
     * @param newPhoneNumber adn number to be stored
     *        Set both newTag and newPhoneNubmer to "" means to replace the old
     *        record with empty one, aka, delete old record
     * @param index is 1-based adn record index to be updated
     * @param pin2 required to update EF_FDN, otherwise must be null
     * @return true for success
     */
    public boolean
    updateAdnRecordsInEfByIndex(int efid, String newTag,
            String newPhoneNumber, int index, String pin2) {

        if (mPhone.getContext().checkCallingOrSelfPermission(
                android.Manifest.permission.WRITE_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException(
                    "Requires android.permission.WRITE_CONTACTS permission");
        }

        if (DBG) logd("updateAdnRecordsInEfByIndex: efid=0x" +
                Integer.toHexString(efid).toUpperCase() + " Index=" + index + " ==> " + "(" +
                Rlog.pii(LOG_TAG, newTag) + "," + Rlog.pii(LOG_TAG, newPhoneNumber) + ")" +
                " pin2=" + Rlog.pii(LOG_TAG, pin2));

        if (!checkLengthOfTag(efid, newTag)) return false;

        checkThread();
        Request updateRequest = new Request();
        synchronized (updateRequest) {
            Message response = mBaseHandler.obtainMessage(EVENT_UPDATE_DONE, updateRequest);
            AdnRecord newAdn = new AdnRecord(newTag, newPhoneNumber);
            if (mAdnCache != null) {
                mAdnCache.updateAdnByIndex(efid, newAdn, index, pin2, response);
                waitForResult(updateRequest);
            } else {
                loge("Failure while trying to update by index due to uninitialised adncache");
            }
        }
        return (boolean) updateRequest.mResult;
    }

    /**
     * Update an ADN-like EF record by record index
     *
     * This is useful for iteration the whole ADN file, such as write the whole
     * phone book or erase/format the whole phonebook. Currently the email field
     * if set in the ADN record is ignored.
     * throws SecurityException if no WRITE_CONTACTS permission
     *
     * @param efid must be one among EF_ADN, EF_FDN, and EF_SDN
     * @param newTag adn tag to be stored
     * @param newPhoneNumber adn number to be stored
     *        Set both newTag and newPhoneNubmer to "" means to replace the old
     *        record with empty one, aka, delete old record
     * @param newEmails is String array storing email address
     * @param newAnrs is String array storing additional numbers
     * @param newAass is String array storing the type of numbers
     *        e.g) Mobile, Home, Work, Home Fax, Other
     * @param index is 1-based adn record index to be updated
     * @param pin2 required to update EF_FDN, otherwise must be null
     * @return true for success
     */
    public boolean
    updateExtentedAdnRecordsInEfByIndex(int efid, String newTag,
            String newPhoneNumber, String[] newEmails, String[] newAnrs, String[] newAass,
            int index, String pin2) {

        if (mPhone.getContext().checkCallingOrSelfPermission(
                android.Manifest.permission.WRITE_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException(
                    "Requires android.permission.WRITE_CONTACTS permission");
        }

        if (DBG) logd("updateExtentedAdnRecordsInEfByIndex: efid=0x" +
                Integer.toHexString(efid).toUpperCase() + " Index=" + index + " ==> " + "(" +
                Rlog.pii(LOG_TAG, newTag) + "," + Rlog.pii(LOG_TAG, newPhoneNumber) + ")" +
                Rlog.pii(LOG_TAG, newEmails) + "," + Rlog.pii(LOG_TAG, newAnrs) + ")" +
                "," + Rlog.pii(LOG_TAG, newAass) + ")" +" pin2=" + Rlog.pii(LOG_TAG, pin2));

        efid = updateEfForIccType(efid);
        if (!checkLengthOfTag(efid, newTag)) return false;

        checkThread();
        Request updateRequest = new Request();
        synchronized (updateRequest) {
            Message response = mBaseHandler.obtainMessage(EVENT_UPDATE_DONE, updateRequest);
            AdnRecord newAdn = null;
            if (efid == IccConstants.EF_PBR) {
                newAdn = new AdnRecord(newTag, newPhoneNumber, newEmails, newAnrs, newAass);
            } else {
                newAdn = new AdnRecord(newTag, newPhoneNumber);
            }
            if (mAdnCache != null) {
                mAdnCache.updateUsimAdnByIndex(efid, newAdn, index, pin2, response);
                waitForResult(updateRequest);
            } else {
                loge("Failure while trying to update by index due to uninitialised adncache");
            }
        }
        return (boolean) updateRequest.mResult;
    }


    /**
     * Get the capacity of records in efid
     *
     * @param efid the EF id of a ADN-like ICC
     * @return  int[3] array
     *            recordSizes[0]  is the single record length
     *            recordSizes[1]  is the total length of the EF file
     *            recordSizes[2]  is the number of records in the EF file
     */
    public int[] getAdnRecordsSize(int efid) {
        if (DBG) logd("getAdnRecordsSize: efid=" + efid);
        checkThread();
        Request getSizeRequest = new Request();
        synchronized (getSizeRequest) {
            //Using mBaseHandler, no difference in EVENT_GET_SIZE_DONE handling
            Message response = mBaseHandler.obtainMessage(EVENT_GET_SIZE_DONE, getSizeRequest);
            IccFileHandler fh = mPhone.getIccFileHandler();
            if (fh != null) {
                fh.getEFLinearRecordSize(efid, response);
                waitForResult(getSizeRequest);
            }
        }

        return getSizeRequest.mResult == null ? new int[3] : (int[]) getSizeRequest.mResult;
    }


    /**
     * Loads the AdnRecords in efid and returns them as a
     * List of AdnRecords
     *
     * throws SecurityException if no READ_CONTACTS permission
     *
     * @param efid the EF id of a ADN-like ICC
     * @return List of AdnRecord
     */
    public List<AdnRecord> getAdnRecordsInEf(int efid) {

        if (mPhone.getContext().checkCallingOrSelfPermission(
                android.Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException(
                    "Requires android.permission.READ_CONTACTS permission");
        }

        int ef_id = updateEfForIccType(efid);
        if (DBG) logd("getAdnRecordsInEF: efid=0x" + Integer.toHexString(ef_id).toUpperCase());

        checkThread();
        Request loadRequest = new Request();
        // try to read PBR record
        if (mIsPbrExist == true) {
            synchronized (loadRequest) {
                Message response = mBaseHandler.obtainMessage(EVENT_LOAD_DONE, loadRequest);
                if (mAdnCache != null) {
                    mAdnCache.requestLoadAllAdnLike(ef_id, mAdnCache.extensionEfForEf(ef_id), response);
                    waitForResult(loadRequest);
                } else {
                    loge("Failure while trying to load from SIM due to uninitialised adncache");
                }
            }
        }

        // if reading PBR record is failed (file not found), re-try ADN records, and keep this boolean variale.
        if (mIsPbrExist == false) {
            ef_id = updateEfForIccType(efid);
            logd("PBR Records were not in this SIM, so re-load this - efid:" + ef_id);
            synchronized (loadRequest) {
                Message response = mBaseHandler.obtainMessage(EVENT_LOAD_PURE_ADN_DONE, loadRequest);
                if (mAdnCache != null) {
                    mAdnCache.requestLoadAllAdnLike(ef_id, mAdnCache.extensionEfForEf(ef_id), response);
                    waitForResult(loadRequest);
                } else {
                    loge("Failure while trying to load from SIM due to uninitialised adncache");
                }
            }
        }

        Request loadSdnRequest = new Request();
        synchronized (loadSdnRequest) {
            Message response = mBaseHandler.obtainMessage(EVENT_LOAD_SDN_DONE, loadSdnRequest);
            if (mAdnCache != null) {
                if (ef_id == IccConstants.EF_PBR && mAdnCache.getSdnSize() <= 0) {
                    mAdnCache.requestLoadAllAdnLike(IccConstants.EF_SDN,
                            mAdnCache.extensionEfForEf(IccConstants.EF_SDN), response);
                    waitForResult(loadSdnRequest);
                    if (loadSdnRequest.mResult != null && loadRequest.mResult != null) {
                        ((List<AdnRecord>) loadRequest.mResult).addAll((List<AdnRecord>) loadSdnRequest.mResult);
                    }
                }
            } else {
                loge("Failure while trying to load from SIM due to uninitialised adncache");
            }
        }

        return (List<AdnRecord>) loadRequest.mResult;
    }

    @UnsupportedAppUsage
    protected void checkThread() {
        // Make sure this isn't the UI thread, since it will block
        if (mBaseHandler.getLooper().equals(Looper.myLooper())) {
            loge("query() called on the main UI thread!");
            throw new IllegalStateException(
                    "You cannot call query on this provder from the main UI thread.");
        }
    }

    protected void waitForResult(Request request) {
        synchronized (request) {
            while (!request.mStatus.get()) {
                try {
                    request.wait();
                } catch (InterruptedException e) {
                    logd("interrupted while trying to update by search");
                }
            }
        }
    }

    private String[] getStringArray(String str) {
        if (str != null) {
            return str.split(",");
        }
        return null;
    }

    @UnsupportedAppUsage
    private int updateEfForIccType(int efid) {
        // Check if we are trying to read ADN records
        AppType type = mPhone.getCurrentUiccAppType();
        if (efid == IccConstants.EF_ADN && mIsPbrExist == true) {
            if (type == AppType.APPTYPE_USIM || type == AppType.APPTYPE_CSIM) {
                return IccConstants.EF_PBR;
            }
        }
        return efid;
    }

    public int getAdnCount() {
        int adnCount = 0;
        AppType type = mPhone.getCurrentUiccAppType();
        if (mAdnCache != null) {
            if (type == AppType.APPTYPE_USIM || type == AppType.APPTYPE_CSIM) {
                adnCount = mAdnCache.getUsimAdnCount();
            } else {
                adnCount = mAdnCache.getAdnCount();
            }
        } else {
            loge("mAdnCache is NULL when getAdnCount.");
        }
        return adnCount;
    }


    public int getAnrCount() {
        int anrCount = 0;
        if (mAdnCache != null) {
            anrCount = mAdnCache.getAnrCount();
        } else {
            loge("mAdnCache is NULL when getAnrCount.");
        }
        return anrCount;
    }

    public int getEmailCount() {
        int emailCount = 0;
        if (mAdnCache != null) {
            emailCount = mAdnCache.getEmailCount();
        } else {
            loge("mAdnCache is NULL when getEmailCount.");
        }
        return emailCount;
    }

    public int getSpareAnrCount() {
        int spareAnrCount = 0;
        if (mAdnCache != null) {
            spareAnrCount = mAdnCache.getSpareAnrCount();
        } else {
            loge("mAdnCache is NULL when getSpareAnrCount.");
        }
        return spareAnrCount;
    }

    public int getSpareEmailCount() {
        int spareEmailCount = 0;
        if (mAdnCache != null) {
            spareEmailCount = mAdnCache.getSpareEmailCount();
        } else {
            loge("mAdnCache is NULL when getSpareEmailCount.");
        }
        return spareEmailCount;
    }

    public int getSupportedAnrSetNum() {
        int maxAnrSpace = 0;
        if (mAdnCache != null) {
            maxAnrSpace = mAdnCache.getSupportedAnrSetNum();
        } else {
            loge("mAdnCache is NULL when getSupportedAnrSetNum.");
        }
        return maxAnrSpace;
    }

    public boolean isApplicationOnIcc(int type) {
        if (mPhone.getCurrentUiccAppType() == IccCardApplicationStatus.AppType.values()[type]) {
            return true;
        }
        return false;
    }

    protected boolean checkLengthOfTag(int efid, String name) {
        if (mTagLen < 0) {
            getAdnRecordsSize(efid);
        }
        if (!TextUtils.isEmpty(name) && mTagLen >= 0) {
            byte[] nameTag = IccUtils.stringToAdnStringField(name);
            if (nameTag.length > mTagLen) {
                return false;
            }
        }
        return true;
    }
}

