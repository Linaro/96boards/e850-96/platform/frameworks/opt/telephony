/*
 * copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.ServiceState;

/**
 * {@hide}
 */
public class UplmnInfoSet {

    private int mMaxPlmnNum;
    ArrayList<UplmnInfo> mList;

    UplmnInfoSet(int maxNum, ArrayList<vendor.samsung_slsi.telephony.hardware.radio.V1_0.UplmnInfo> uplmnInfos) {
        mMaxPlmnNum = maxNum;
        mList = new ArrayList<UplmnInfo>();
        for (int i = 0; i < uplmnInfos.size(); i++) {
            mList.add(new UplmnInfo(uplmnInfos.get(i).index, uplmnInfos.get(i).numeric, uplmnInfos.get(i).act));
        }
    }

    public ArrayList<UplmnInfo> getList() {
        return mList;
    }

    public int getMaxPlmnNum() {
        return mMaxPlmnNum;
    }

    public static class UplmnInfo {

        private String mIndexOfOperator;
        private String mPlmn;
        private String mRat;

        UplmnInfo(String operatorIndex, String plmn, String rat) {

            mIndexOfOperator = operatorIndex;
            mPlmn = plmn;
            mRat = rat;
        }

        UplmnInfo(int operatorIndex, String plmn, int rat) {

            mIndexOfOperator = "" + operatorIndex;
            mPlmn = plmn;
            mRat = "" + rat;
        }

        public String getOperatorIndex() {
            return mIndexOfOperator;
        }

        public String getPlmn() {
            return mPlmn;
        }

        public String getRat() {
            return mRat;
        }

        @Override
        public String toString() {
            return "UplmnInfo " + mIndexOfOperator + "/" + mPlmn + "/" + mRat;
        }
    }
}
