package com.android.internal.telephony.dataconnection;

public class NasTimerResponse {
    public int type = 0;
    public int status = 0;
    public int value = 0;
    public String apn = "";

    public static final int NAS_TIMER_TYPE_PLMN = 0;
    public static final int NAS_TIMER_TYPE_T3346 = 1;
    public static final int NAS_TIMER_TYPE_T3396 = 2;
    public static final int NAS_TIMER_STATUS_STARTED = 1;
    public static final int NAS_TIMER_STATUS_STOPPED = 2;
    public static final int NAS_TIMER_STATUS_EXPIRED = 3;
}
