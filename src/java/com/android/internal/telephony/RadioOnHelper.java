package com.android.internal.telephony;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.UserHandle;
import android.provider.Settings;

import android.telephony.Rlog;
import android.telephony.ServiceState;

public class RadioOnHelper {

    public interface Callback {
        void onComplete(boolean isRadioReady);
    }

    static final String LOG_TAG = "RadioOnHelper";

    public static final int MSG_SERVICE_STATE_CHANGED = 1;
    public static final int MSG_RETRY_TIMEOUT = 2;

    private final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SERVICE_STATE_CHANGED:
                    // do not use msg and mPhone.getServiceState()
                    onServiceStateChanged(mPhone.getServiceStateTracker().mSS);
                    break;
                case MSG_RETRY_TIMEOUT:
                    onRetryTimeout();
                    break;
                default:
                    loge("handleMessage: unexpected message: " + msg.what);
                    break;
            }
        }
    };

    private static int MAX_NUM_RETRIES = 5;
    private static long TIME_BETWEEN_RETRIES_MILLIS = 5000;  // msec

    private final Context mContext;
    private Phone mPhone;
    private final Callback mCallback;
    private int mNumRetriesSoFar;

    public RadioOnHelper(Phone phone, Callback callback) {
        mPhone = phone;
        mContext = phone.getContext();
        mCallback = callback;

        waitForRadioOn();
        powerOnRadio();
    }

    private void powerOnRadio() {
        log("powerOnRadio().");

        // If airplane mode is on, we turn it off the same way that the Settings activity turns it
        // off.
        if (Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) > 0) {
            log("==> Turning off airplane mode.");

            // Change the system setting
            Settings.Global.putInt(mContext.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0);

            Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            intent.putExtra("state", false);
            mContext.sendBroadcastAsUser(intent, UserHandle.ALL);
        }
    }

    public void waitForRadioOn() {
        startRetryTimer();

        unregisterForServiceStateChanged();
        mPhone.registerForServiceStateChanged(mHandler, MSG_SERVICE_STATE_CHANGED, null);
    }

    private void startRetryTimer() {
        cancelRetryTimer();
        mHandler.sendEmptyMessageDelayed(MSG_RETRY_TIMEOUT, TIME_BETWEEN_RETRIES_MILLIS);
    }

    private void cancelRetryTimer() {
        mHandler.removeMessages(MSG_RETRY_TIMEOUT);
    }

    private void unregisterForServiceStateChanged() {
        if (mPhone != null) {
            mPhone.unregisterForServiceStateChanged(mHandler);
        }
        mHandler.removeMessages(MSG_SERVICE_STATE_CHANGED);
    }

    private void onServiceStateChanged(ServiceState state) {
        log("onServiceStateChanged(), new state = " +
            ServiceState.rilServiceStateToString(state.getState()));

        if (isOkToCall(state.getState())) {
            log("onServiceStateChanged: ok to call!");
            mCallback.onComplete(true);
            cleanup();
        } else {
            // The service state changed, but we're still not ready to call yet.
            log("onServiceStateChanged: not ready to call yet, keep waiting.");
        }
    }

    private boolean isOkToCall(int serviceState) {
        // need to check STATE_OUT_OF_SERVICE?
        boolean ret = mPhone.isRadioAvailable() && serviceState == ServiceState.STATE_IN_SERVICE;

        if (!ret) {
            log("isOkToCall phone.isRadioAvailable()= " + mPhone.isRadioAvailable() +
                ", serviceState= " + ServiceState.rilServiceStateToString(serviceState));
        }
        return ret;
    }

    private void cleanup() {
        log("cleanup()");

        unregisterForServiceStateChanged();
        cancelRetryTimer();

        mPhone = null;
        mNumRetriesSoFar = 0;
    }

    private void onRetryTimeout() {
        int serviceState = mPhone.getServiceState().getState();
        log("onRetryTimeout(): service state = " + ServiceState.rilServiceStateToString(serviceState) +
            ", retries = " + mNumRetriesSoFar);

        if (isOkToCall(serviceState)) {
            log("onRetryTimeout: Radio is on. Cleaning up.");

            mCallback.onComplete(true);
            cleanup();
        } else {
            mNumRetriesSoFar++;
            log("mNumRetriesSoFar is now " + mNumRetriesSoFar);

            if (mNumRetriesSoFar > MAX_NUM_RETRIES) {
                log("Hit MAX_NUM_RETRIES; giving up.");
                mCallback.onComplete(false);
                cleanup();
            } else {
                log("Trying (again) to turn on the radio.");
                mPhone.setRadioPower(true);
                startRetryTimer();
            }
        }
    }

    private void log(String msg) {
        Rlog.d(LOG_TAG, (mPhone != null ? "[" + mPhone.getPhoneId() + "] " : "mPhone is null ") + msg);
    }

    private void loge(String msg) {
        Rlog.e(LOG_TAG, (mPhone != null ? "[" + mPhone.getPhoneId() + "] " : "mPhone is null ") + msg);
    }
}
