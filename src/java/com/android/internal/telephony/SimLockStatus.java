/*
 * copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.List;




/**
 * {@hide}
 */
public class SimLockStatus {

    private int mPolicy;
    private int mStatus;
    private int mLockType;
    private int mMaxRetryCount;
    private int mRemainCount;
    private ArrayList<String> mLockCode;

    SimLockStatus(int policy, int status, int lockType, int maxRetrynCount, int remainCount, List<String> lockCode) {
        mPolicy = policy;
        mStatus = status;
        mLockType = lockType;
        mMaxRetryCount = maxRetrynCount;
        mRemainCount = remainCount;
        mLockCode = new ArrayList<String>();
        if (lockCode != null) {
            mLockCode.addAll(lockCode);
        }
    }

    public int getPolicy() { return mPolicy; }
    public int getStatus() { return mStatus; }
    public int getLockType() { return mLockType; }
    public int getMaxRetryCount() { return mMaxRetryCount; }
    public int getRemainCount() { return mRemainCount; }
    public List<String> getLockCode() { return mLockCode; }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SimLockStatus={");
        sb.append(" policy=" + mPolicy);
        sb.append(" status=" + mStatus);
        sb.append(" lockType=" + mLockType);
        sb.append(" maxRetrynCount=" + mMaxRetryCount);
        sb.append(" remainCount=" + mRemainCount);
        sb.append(" lockCodeSize=" + mLockCode.size());
        sb.append("}");

        return sb.toString();
    }
}

