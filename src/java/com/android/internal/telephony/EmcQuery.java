package com.android.internal.telephony;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.Registrant;
import android.os.RegistrantList;
import android.telephony.CarrierConfigManager;
import android.telephony.DisconnectCause;
import android.telephony.emergency.EmergencyNumber;
import android.telephony.ims.ImsCallProfile;
import android.telephony.PhoneNumberUtils;
import android.telephony.Rlog;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.ims.ImsManager;

import com.android.internal.telephony.emergency.EmergencyNumberTracker;
import com.android.internal.telephony.imsphone.ImsPhone;
import com.android.internal.telephony.imsphone.ImsPhoneCall;
import com.android.internal.telephony.imsphone.ImsPhoneConnection;
import com.android.internal.telephony.PhoneInternalInterface.DialArgs;

import java.util.List;

public class EmcQuery extends Handler {
    private static final String LOG_TAG = "EmcQuery";

    protected static final int EVENT_EMC_ACT_INFO            = 100;
    protected static final int EVENT_EMC_TIMEOUT             = 101;

    // EMERGENCY CALL ACT INFO
    static final int EMC_ACT_INFO_NO_MORE_USED     = 0;
    static final int EMC_ACT_INFO_CAN_BE_USED      = 1;
    static final int EMC_ACT_INFO_RETRY_EMC_STATUS = 2;

    public static final int ECALL_SUPPORT_RAT_3GPP  = 0;
    public static final int ECALL_SUPPORT_RAT_3GPP2 = 1;
    public static final int ECALL_SUPPORT_RAT_ALL   = 2;

    public enum EmcStatus {
        CALL_NONE,
        CALL_START,
        CALL_END,
        CALL_CANCELED,
        CALL_FAIL,
        CALL_STARTED,
        CALL_RETRY;

        public boolean isDialingEmc() {
            return (this == CALL_START || this == CALL_FAIL);
        }
    }

    public static final int RAT_NOT_SPECIFIED = -1;
    public static final int RAT_GSM_ONLY = 0;
    public static final int RAT_GSM_PREF = 1;
    public static final int RAT_CDMA_PREF = 2;

    public static final int CS_PREF = 1;
    public static final int VOLTE_PREF = 2;
    public static final String CS_PREF_VALUE = "001";

    private EmcStatus mEmcStatus = EmcStatus.CALL_NONE;
    private int mTriggeredRatInfo = 0xFF; // RAT_NOT_SPECIFIED
    private int mRatPref = RAT_NOT_SPECIFIED;
    private Connection mEmcConnection = null;
    private Connection mLastConnection = null;
    private GsmCdmaPhone mDefaultPhone = null;
    private String mDialString = "";
    private DialArgs mDialArgs = null;
    private boolean mEmcTimeout = false;
    private int mPhoneId = 0;

    public RegistrantList mRefreshConnectionRegistrantList = new RegistrantList();;

    private static EmcQuery sInstance;

    public EmcQuery() {
    }

    public static synchronized EmcQuery getInstance() {
        if (sInstance == null) {
            sInstance = new EmcQuery();
        }
        return sInstance;
    }

    public void setDefaultPhone(GsmCdmaPhone defaultPhone) {
        if (mDefaultPhone != null) {
            mDefaultPhone.mCi.unregisterForEmergencyActInfo(this);
        }

        mDefaultPhone = defaultPhone;
        mPhoneId = defaultPhone.getPhoneId();
        mDefaultPhone.mCi.registerForEmergencyActInfo(this, EVENT_EMC_ACT_INFO, null);
    }

    public void setDialString(String dialString) {
        mDialString = dialString;
    }

    public void setDialArgs(DialArgs dialArgs) {
        mDialArgs = dialArgs;
    }

    public DialArgs getImsDialArgs(boolean isVoWifiCall) {
        if (mDialArgs != null) {
            ImsPhone.ImsDialArgs.Builder imsDialArgsBuilder;
            if (!(mDialArgs instanceof ImsPhone.ImsDialArgs)) {
                imsDialArgsBuilder = ImsPhone.ImsDialArgs.Builder.from(mDialArgs);
            } else {
                imsDialArgsBuilder = ImsPhone.ImsDialArgs.Builder.from((ImsPhone.ImsDialArgs) mDialArgs);
            }
            Bundle extras = mDialArgs.intentExtras;
            if (extras == null) {
                extras = new Bundle();
            }
            extras.putBoolean("isEmergency", true);
            if (isVoWifiCall) {
                extras.putString(ImsCallProfile.EXTRA_CALL_RAT_TYPE,
                        String.valueOf(ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN));
            }
            imsDialArgsBuilder.setIntentExtras(extras);
            return imsDialArgsBuilder.build();
        }

        Bundle extras = new Bundle();
        extras.putBoolean("isEmergency", true);
        return new DialArgs.Builder<>().setIntentExtras(extras).build();
    }

    public DialArgs getDialArgs() {
        if (mDialArgs != null) {
            return new DialArgs.Builder<>().setIntentExtras(mDialArgs.intentExtras).build();
        }
        return new DialArgs.Builder<>().setIntentExtras(null).build();
    }

    public synchronized void setEmcStatus(EmcStatus status) {
        mEmcStatus = status;
    }

    public synchronized EmcStatus getEmcStatus() {
        return mEmcStatus;
    }

    public synchronized void processEmcStatus(EmcStatus status)
        throws CallStateException {
        EmcStatus oldStatus = getEmcStatus();
        setEmcStatus(status);

        logi("processEmcStatus oldStatus: " + oldStatus + " newStatus: " + status);

        if (oldStatus == status) {
            return;
        }

        Phone imsPhone = mDefaultPhone.getImsPhone();

        if (status == EmcStatus.CALL_NONE) {
            mTriggeredRatInfo = 0xFF; // RAT_NOT_SPECIFIED;
            mRatPref = RAT_NOT_SPECIFIED;
            disconnectFakeConn();
            mLastConnection = null;
            mDialArgs = null;
            disconnectEmcConnection();
            if (imsPhone != null) {
                ((ImsPhone)imsPhone).clearDisconnected();
            }
            mDefaultPhone.clearDisconnected();
            resetEmcTimer();
        } else if (status == EmcStatus.CALL_START) {
            if (oldStatus == EmcStatus.CALL_NONE || mDefaultPhone.isInEcm()) {
                if (mRatPref == RAT_GSM_ONLY) {
                    mTriggeredRatInfo = ServiceState.RIL_RADIO_TECHNOLOGY_GSM;
                    logi("RAT_GSM_ONLY mTriggeredRatInfo = " + mTriggeredRatInfo);
                } else if (mRatPref == RAT_GSM_PREF) {
                    mTriggeredRatInfo = ServiceState.RIL_RADIO_TECHNOLOGY_GSM;
                    logi("RAT_GSM_PREF mTriggeredRatInfo = " + mTriggeredRatInfo);
                } else if (mRatPref == RAT_CDMA_PREF) {
                    mTriggeredRatInfo = ServiceState.RIL_RADIO_TECHNOLOGY_1xRTT;
                    logi("RAT_CDMA_PREF mTriggeredRatInfo = " + mTriggeredRatInfo);
                }
                logi("mTriggeredRatInfo = " + mTriggeredRatInfo);
                mDefaultPhone.mCi.setEmcStatus(status.ordinal(), mTriggeredRatInfo, null);

                // run timer to cancel EMC
                int delay = getEmcConnectionDelay();
                if (delay > 0 && mDefaultPhone.isWifiCallingEnabled()) {
                    sendMessageDelayed(obtainMessage(EVENT_EMC_TIMEOUT), delay);
                }
            } else {
                setEmcStatus(oldStatus);
                throw new CallStateException("cannot start EMC dial old status is not CALL_NONE");
            }
        } else if (status == EmcStatus.CALL_RETRY) {
            if (oldStatus == EmcStatus.CALL_START) {
                if (mDefaultPhone.isWifiCallingEnabled()) {
                    logi("Received retry emc status, but VoWifi is enabled. Stop emc status");
                    triggerVoWifiCallAsEmergency();
                    try {
                        processEmcStatus(EmcStatus.CALL_END);
                    } catch (CallStateException ex) {
                        // it may not be entered in this catch.
                    }
                } else {
                    setEmcStatus(EmcStatus.CALL_START);
                    logi("retry emc status mTriggeredRatInfo = " + mTriggeredRatInfo);
                    mDefaultPhone.mCi.setEmcStatus(oldStatus.ordinal(), mTriggeredRatInfo, null);
                }
            }
        } else if (status == EmcStatus.CALL_END) {
            mDefaultPhone.mCi.setEmcStatus(status.ordinal(), 0xFF /* RAT_NOT_SPECIFIED */, null);
        } else if (status == EmcStatus.CALL_CANCELED) {
            // do nothing
        } else if (status == EmcStatus.CALL_FAIL) {
            if ((mEmcTimeout || useVoWifiCallAfterCallFail()) && mDefaultPhone.isWifiCallingEnabled()) {
                triggerVoWifiCallAsEmergency();
                try {
                    processEmcStatus(EmcStatus.CALL_END);
                } catch (CallStateException ex) {
                }
                return;
            }
            mDefaultPhone.mCi.setEmcStatus(status.ordinal(), mTriggeredRatInfo, null);
        } else if (status == EmcStatus.CALL_STARTED) {
        }
    }

    public void handleEmcActInfo(int rat, int actStatus) {
        logi("handleEmcActInfo support rat: " + rat + " actStatus: " + actStatus);
        EmcStatus status = getEmcStatus();

        if (actStatus == EMC_ACT_INFO_NO_MORE_USED) {
            if (status.isDialingEmc() && mDefaultPhone.isWifiCallingEnabled()) {
                triggerVoWifiCallAsEmergency();
                try {
                    processEmcStatus(EmcStatus.CALL_END);
                } catch (CallStateException ex) {
                }
                return;
            }

            if (status == EmcStatus.CALL_STARTED || status.isDialingEmc()) {
                // hangup the call, if there's a call.
                // need implementation
                // stop the redial, and show pop up on FAIL case only?
                // need implementation?
                try {
                    //After start EMC, received no more used.
                    logi("Try to start EMC, but received no more used.");
                    processEmcStatus(EmcStatus.CALL_END);
                } catch (CallStateException ex) {
                    // it may not be entered in this catch.
                }
            }
            try {
                processEmcStatus(EmcStatus.CALL_NONE);
            } catch (CallStateException ex) {
                // it may not be entered in this catch.
            }
        } else if (actStatus == EMC_ACT_INFO_CAN_BE_USED && status.isDialingEmc()) {
            try {
                mTriggeredRatInfo = rat;
                Connection conn = null;

                if (!ServiceState.isLte(rat)) {
                    mDefaultPhone.phoneObjectUpdater(rat);
                }

                processEmcStatus(EmcStatus.CALL_STARTED);
                if (ServiceState.isLte(rat)) {
                    // TBD mDefaultPhone.showHDIcon(true);
                    // try dial over VoLTE
                    try {
                        Phone imsPhone = mDefaultPhone.getImsPhone();

                        conn = imsPhone.dial(mDialString, getImsDialArgs(false));
                    } catch (CallStateException ex2) {
                        loge("EMC VoLTE dial failed: " + ex2);
                        processEmcStatus(EmcStatus.CALL_END);
                    }
                } else if (ServiceState.isGsm(rat) || ServiceState.isCdma(rat)) {
                    // TBD showHDIcon(false);
                    // try dial over CS
                    try {
                        conn = mDefaultPhone.dialInternal(mDialString, getDialArgs(), true);
                    } catch (CallStateException ex2) {
                        loge("EMC CS dial failed: " + ex2);
                        processEmcStatus(EmcStatus.CALL_END);
                    }
                } else {
                    processEmcStatus(EmcStatus.CALL_END);
                }

                if (conn != null) {
                    mLastConnection = conn;
                    logi("handleEmcActInfo notifyRefreshConnection: " + conn);
                    notifyRefreshConnection(conn);
                }
            } catch (CallStateException ex) {
                loge("EMC handleEmcActInfo failed: " + ex);
                try {
                    processEmcStatus(EmcStatus.CALL_END);
                } catch (CallStateException ex2) {
                    // it may not be entered in this catch.
                }
            }
        } else if (actStatus == EMC_ACT_INFO_RETRY_EMC_STATUS && status.isDialingEmc()) {
            logi("Retry processEmcStatus: ");
            try {
                processEmcStatus(EmcStatus.CALL_RETRY);
            } catch (CallStateException ex) {
                loge("Fail to retry processEmcStatus: " + ex);
                try {
                    processEmcStatus(EmcStatus.CALL_END);
                } catch (CallStateException ex2) {
                    // it may not be entered in this catch.
                }
            }
        }
    }

    public void abortEmc() {
        if (getEmcStatus().isDialingEmc()) {
            try {
                logi("abortEmc send CALL_END");
                processEmcStatus(EmcStatus.CALL_END);
            } catch (CallStateException ex) {
            }
        }
    }

    public void disconnectFakeConn() {
        mDefaultPhone.disconnectFakeConn();
    }

    public void disconnectEmcConnection() {
        if (mEmcConnection != null) {
            if (mEmcConnection instanceof ImsPhoneConnection) {
                ImsPhoneConnection conn = (ImsPhoneConnection) mEmcConnection;
                conn.update(null, ImsPhoneCall.State.DISCONNECTED);
                conn.onDisconnect();
                conn.getCall().detach(conn);
                conn = null;
            } else if (mEmcConnection instanceof GsmCdmaConnection) {
                // it can be already cleared by LAST_CALL_FAIL_CAUSE.
                GsmCdmaConnection conn = (GsmCdmaConnection) mEmcConnection;
                if (!conn.onDisconnect(DisconnectCause.NORMAL)) {
                    conn.getCall().detach(conn);
                    conn = null;
                }
            }
        }
        mEmcConnection = mLastConnection;
    }

    public void setPrefRatForEmc(String number, boolean isImsRegi) {
        Rlog.i(LOG_TAG, "setPrefRatForEmc");
        String value = "";
        int prefRat = RAT_NOT_SPECIFIED;
        if (isImsRegi) {
            // Value: the combination of each digit, Support(1)/Unsupport(0)
            //  - 1st: Emergency Number over IMS Emergency bearer
            //  - 2nd: Emergency Number over IMS Nomral bearer
            //  - 3rd: Emergency Number over CS
            EmergencyNumberTracker tracker = mDefaultPhone.getEmergencyNumberTracker();
            if (tracker!= null) {
                List<EmergencyNumber> nums = tracker.getEmergencyNumberList();
                for (EmergencyNumber num : nums) {
                    if (num.getNumber().equals(number)) {
                        Rlog.i(LOG_TAG, "setPrefRatForEmc num: " + num + " number: " + number);
                        List<String> urns = num.getEmergencyUrns();
                        for (String urn : urns) {
                            if (urn.startsWith(EmergencyNumberTracker.EMERGENCY_CALL_VALUE_URN)) {
                                value = urn.substring(EmergencyNumberTracker.EMERGENCY_CALL_VALUE_URN.length());
                            }
                        }
                    }
                }
            }
            if (value.equals(CS_PREF_VALUE)) {
                prefRat = CS_PREF;
            } else {
                prefRat = VOLTE_PREF;
            }
            setRatForEmc(prefRat);
        } else {
            setPrefRatForEmc(number);
        }
    }

    public void setPrefRatForEmc(String number) {
        String gsmPriorEmergencyNumbers = "112,000,08,118,911,999";
        String cdmaPriorEmergencyNumbers = "110,119,120,122";

        int prefRat = RAT_NOT_SPECIFIED;

        // Strip the separators from the number before comparing it
        // to the list.
        number = PhoneNumberUtils.extractNetworkPortionAlt(number);
        logd("setPrefRatForEmc number: " +  number);

        for(String emergencyNum : gsmPriorEmergencyNumbers.split(",")) {
            if (number.equals(emergencyNum)) {
                prefRat = RAT_GSM_PREF;
            }
        }
        for(String emergencyNum : cdmaPriorEmergencyNumbers.split(",")) {
            if (number.equals(emergencyNum)) {
                prefRat = RAT_CDMA_PREF;
            }
        }
        setRatForEmc(prefRat);
    }

    public int getRatForEmc() {
        return mRatPref;
    }

    public void setRatForEmc(int ratPref) {
        mRatPref = ratPref;
    }

    private void triggerVoWifiCallAsEmergency() {
        // need phoneObjectUpdater?
        Connection conn = null;

        try {
            logi("triggerVoWifiCallAsEmergency trigger VoWifi call");
            Phone imsPhone = mDefaultPhone.getImsPhone();
            conn = imsPhone.dial(mDialString, getImsDialArgs(true));
        } catch (CallStateException ex) {
            loge("EMC VoWifi dial failed: " + ex);
        }

        if (conn != null) {
            logi("triggerVoWifiCallAsEmergency notifyRefreshConnection: " + conn);
            notifyRefreshConnection(conn);
        }
    }

    public void registerForRefreshConnection(Handler h, int what, Object obj) {
        mRefreshConnectionRegistrantList.add(new Registrant(h, what, obj));
    }

    public void unregisterForRefreshConnection(Handler h) {
        mRefreshConnectionRegistrantList.remove(h);
    }

    protected void notifyRefreshConnection(Connection cn) {
        AsyncResult ar = new AsyncResult(null, cn, null);
        mRefreshConnectionRegistrantList.notifyRegistrants(ar);
    }

    public boolean useVoWifiCallAfterCallFail() {
        CarrierConfigManager configMgr = (CarrierConfigManager)
                mDefaultPhone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle b = configMgr.getConfigForSubId(mDefaultPhone.getSubId());
        if (b != null) {
            return b.getBoolean(CarrierConfigManager.KEY_USE_VOWIFI_CALL_AFTER_CALL_FAIL_SIM);
        }
        return false;
    }

    public int getEmcConnectionDelay() {
        int delay = 0;
        CarrierConfigManager configMgr = (CarrierConfigManager)
                mDefaultPhone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle b = configMgr.getConfigForSubId(mDefaultPhone.getSubId());
        if (b != null) {
            delay = b.getInt(CarrierConfigManager.KEY_EMC_CONNECTION_LIMIT_INT_SIM);
        }
        return delay;
    }

    private void resetEmcTimer() {
        removeMessages(EVENT_EMC_TIMEOUT);
        mEmcTimeout = false;
    }

    @Override
    public void handleMessage(Message msg) {
        AsyncResult ar;
        switch (msg.what) {
            case EVENT_EMC_ACT_INFO: {
                ar = (AsyncResult) msg.obj;
                if (ar.exception != null) {
                    logd("EVENT_EMC_ACT_INFO has exception.");
                    try {
                        processEmcStatus(EmcStatus.CALL_NONE);
                    } catch (CallStateException ex) {
                        // it may not be entered in this catch.
                    }
                } else {
                    int[] result = (int[]) ar.result;
                    if (result != null && result.length == 2) {
                        int rat = result[0];
                        int actStatus = result[1];
                        handleEmcActInfo(rat, actStatus);
                    } else {
                        logd("EVENT_EMC_ACT_INFO has wrong result.");
                        try {
                            processEmcStatus(EmcStatus.CALL_NONE);
                        } catch (CallStateException ex) {
                            // it may not be entered in this catch.
                        }
                    }
                }
                break;
            }
            case EVENT_EMC_TIMEOUT: {
                logd("EVENT_EMC_TIMEOUT");
                mEmcTimeout = true;

                EmcStatus emcStatus = getEmcStatus();
                if (emcStatus.isDialingEmc()) {
                    try {
                        processEmcStatus(EmcStatus.CALL_FAIL);
                    } catch (CallStateException ex) {
                        // it may not be entered in this catch.
                    }
                }
                break;
            }
            default:
                throw new RuntimeException("unexpected event not handled");
        }
    }

    private void logi(String s) {
        Rlog.i(LOG_TAG, "[" + mPhoneId + "] " + s);
    }

    private void logd(String s) {
        Rlog.d(LOG_TAG, "[" + mPhoneId + "] " + s);
    }

    private void loge(String s) {
        Rlog.e(LOG_TAG, "[" + mPhoneId + "] " + s);
    }
}
